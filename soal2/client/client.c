#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <ctype.h>
#define PORT 8080
  
int main(int argc, char const *argv[]) {
    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char buffer[1024] = {0};

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
  
    memset(&serv_addr, '0', sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
      
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }

    while(1) {
        char input[100];
        scanf("%s", input);
        send(sock, input, strlen(input), 0);
        if (strcmp(input, "register") == 0){

            username:
                memset(buffer, 0, 1024);
                printf("Username: ");
                scanf("%s", input);
                send(sock, input, strlen(input), 0);

                valread = read(sock, buffer, 1024);
                if (strcmp(buffer, "Username sudah ada") == 0){
                    printf("%s\n", buffer);
                    goto username;
                }else
                    printf("%s\n", buffer);

            int uppercase, lowercase, number;
            password:
                memset(buffer, 0, 1024);
                uppercase = 0;
                lowercase = 0;
                number = 0;
                printf("Password: ");
                scanf("%s", input);
                if (strlen(input) < 6){
                    printf("\nPassword minimal terdiri dari 6 karakter\n\n");
                    goto password;
                }
                for (int i = 0; i<strlen(input); i++){
                    // printf("%c ", input[i]);
                    if (input[i]>=48 && input[i]<=57)
                        number = 1;
                    if (input[i]>=65 && input[i]<=90)
                        uppercase = 1;
                    if (input[i]>=97 && input[i]<=122)
                        lowercase = 1;
                    // printf("number = %d, uppercase = %d, lowercase = %d\n", number, uppercase, lowercase);
                }
                if (number == 0 || uppercase == 0 || lowercase == 0){
                    printf("\nPassword minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil\n\n");
                    goto password;
                }
                send(sock, input, strlen(input), 0);
                printf("Akun terdaftar\n");
        }

        else if (strcmp(input, "login") == 0){
            
            login_username:
                memset(buffer, 0, 1024);
                printf("Username: ");
                scanf("%s", input);
                send(sock, input, strlen(input), 0);

                valread = read(sock, buffer, 1024);
                if (strcmp(buffer, "Username tidak ada") == 0){
                    printf("%s\n", buffer);
                    goto login_username;
                }

            login_password:
                memset(buffer, 0, 1024);
                printf("Password: ");
                scanf("%s", input);
                send(sock, input, strlen(input), 0);
                valread = read(sock, buffer, 1024);
                if (strcmp(buffer, "Password Salah") == 0){
                    printf("%s\n", buffer);
                    goto login_password;
                }else
                    printf("%s\n", buffer);
        }

        else if (strcmp(input, "add") == 0){
            memset(buffer, 0, 1024);
            valread = read(sock, buffer, 1024);

            if (strcmp(buffer, "Mohon Login Terlebih Dahulu") == 0){
                printf("%s\n", buffer);
            }else{
                add_judul:
                    memset(buffer, 0, 1024);
                    scanf("%s", input);
                    send(sock, input, strlen(input), 0);
                    valread = read(sock, buffer, 1024);
                    if (strcmp(buffer, "Judul Problem sudah ada") == 0){
                        printf("%s\n", buffer);
                        goto add_judul;
                    }else{
                        printf("%s\n", buffer);
                    }
                
                add_filepath_description:
                    memset(buffer, 0, 1024);
                    scanf("%s", input);
                    send(sock, input, strlen(input), 0);

                add_filepath_input:
                    memset(buffer, 0, 1024);
                    scanf("%s", input);
                    send(sock, input, strlen(input), 0);

                add_filepath_output:
                    memset(buffer, 0, 1024);
                    scanf("%s", input);
                    send(sock, input, strlen(input), 0);
    
            }
        }
        else if (strcmp(input, "see") == 0){
            if (strcmp(buffer, "Mohon Login Terlebih Dahulu") == 0){
                printf("%s\n", buffer);
            }else{
                memset(buffer, 0, 1024);
                valread = read(sock, buffer, 1024);
                char output[256];
                char temp[256];

                memset(output, 0, sizeof(output));
                memset(temp, 0, sizeof(temp));

                strcpy(temp, buffer);

                for (int i = 0; i<strlen(temp); i++){
                    if (temp[i] == ' ') continue;
                    else if (temp[i+1] == ' ' && (temp[i] != ' ' && temp[i] != '\t')){
                        strncat(output, &temp[i], 1);
                        strcat(output, " by ");
                        continue;
                    }
                    strncat(output, &temp[i], 1);
                }
                printf("%s\n", output);
            }
            
        }
        else if (strcmp(input, "download") == 0){
            memset(buffer, 0, 1024);
            valread = read(sock, buffer, 1024);

            if (strcmp(buffer, "Mohon Login Terlebih Dahulu") == 0){
                printf("%s\n", buffer);
            }else{
                printf("%s\n", buffer);
                memset(buffer, 0, 1024);
                printf("Judul Problem: ");
                scanf("%s", input);
                send(sock, input, strlen(input), 0);
            }
        }

        else if (strcmp(input, "submit") == 0){
            memset(buffer, 0, 1024);
            valread = read(sock, buffer, 1024);
            if (strcmp(buffer, "Mohon Login Terlebih Dahulu") == 0){
                printf("%s\n", buffer);
            }else{
                printf("Judul Problem: ");
                scanf("%s", input);
                send(sock, input, strlen(input), 0);

                printf("Output File: ");
                scanf("%s", input);
                send(sock, input, strlen(input), 0);

                memset(buffer, 0, 1024);
                valread = read(sock, buffer, 1024);
                printf("%s\n\n", buffer);
                
            }
        }
        else if (strcmp(input, "exit") == 0){
            exit(0);
        }
        else{
            valread = read(sock, buffer, 1024);
            printf("%s\n", buffer);
        }

        memset(buffer, 0, 1024);
    }
}
