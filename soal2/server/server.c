#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>
#define PORT 8080

char temp_judul[100];
int ac = 1;
int queue[3];
int count = 0;

void insertIntoQueue(int x){
    if (count == 3){
        fprintf(stderr, "No more space for client\n");
    }
    queue[count] = x;
    count++;
}

int removeFromQueue(){
    if (count == 0){
        fprintf(stderr, "No elements to extract from queue\n");
        return -1;
    }
    int res = queue[0];
    int i;
    for (i = 0; i<count-1; i++){
        queue[i] = queue[i+1];
    }
    count--;
    return res;
}


void *make_directory(void *arg){
    int check = mkdir(arg, 0777);

    if (!check)
        printf("Directory created\n");
    else{
        printf("Unable to create directory\n");
        exit(1);
    }
}

void *add_description_file(void *arg){
    char ch;
    char *file_name = (char *)arg;
    char path[100];
    FILE *source, *target;

    strcpy(path, "../Client/");
    strcat(path, file_name);
    source = fopen(path, "r");
    if (source == NULL){
        printf("Fail to add files");
        exit(EXIT_FAILURE);
    }

    char target_path[100];
    strcpy(target_path, temp_judul);
    strcat(target_path, "/description.txt");

    target = fopen(target_path, "w");

    if (target == NULL){
        fclose(source);
        printf("Fail to add files");
        exit(EXIT_FAILURE);
    }

    while ((ch = fgetc(source)) != EOF)
        fputc(ch, target);
    
    fclose(source);
    fclose(target);
}

void *add_input_file(void *arg){
    char ch;
    char *file_name = (char *)arg;
    char path[100];
    FILE *source, *target;

    strcpy(path, "../Client/");
    strcat(path, file_name);
    source = fopen(path, "r");
    if (source == NULL){
        printf("Fail to add files");
        exit(EXIT_FAILURE);
    }

    char target_path[100];
    strcpy(target_path, temp_judul);
    strcat(target_path, "/input.txt");

    target = fopen(target_path, "w");

    if (target == NULL){
        fclose(source);
        printf("Fail to add files");
        exit(EXIT_FAILURE);
    }

    while ((ch = fgetc(source)) != EOF)
        fputc(ch, target);
    
    fclose(source);
    fclose(target);
}

void *add_output_file(void *arg){
    char ch;
    char *file_name = (char *)arg;
    char path[100];
    FILE *source, *target;

    strcpy(path, "../Client/");
    strcat(path, file_name);
    source = fopen(path, "r");
    if (source == NULL){
        printf("Fail to add files");
        exit(EXIT_FAILURE);
    }

    char target_path[100];
    strcpy(target_path, temp_judul);
    strcat(target_path, "/output.txt");

    target = fopen(target_path, "w");

    if (target == NULL){
        fclose(source);
        printf("Fail to add files");
        exit(EXIT_FAILURE);
    }

    while ((ch = fgetc(source)) != EOF)
        fputc(ch, target);
    
    fclose(source);
    fclose(target);
}

void *download_file(void *arg){
    char ch;
    char *folder_name = (char *)arg;
    char path[100];
    FILE *source, *target;

    pthread_t tid;
    strcpy(path, "../Client/");
    strcat(path, folder_name);
    pthread_create(&tid, NULL, &make_directory, path);
    pthread_join(tid, NULL);

    strcat(path, "/description.txt");
    target = fopen(path, "w");

    strcpy(path, folder_name);
    strcat(path, "/description.txt");
    source = fopen(path, "r");

    while ((ch = fgetc(source)) != EOF)
        fputc(ch, target);
    
    fclose(source);
    fclose(target);

    strcpy(path, "../Client/");
    strcat(path, folder_name);
    strcat(path, "/input.txt");
    target = fopen(path, "w");

    strcpy(path, folder_name);
    strcat(path, "/input.txt");
    source = fopen(path, "r");

    while ((ch = fgetc(source)) != EOF)
        fputc(ch, target);
    
    fclose(source);
    fclose(target);
}

void *check_submit(void *arg){
    char *client_path = (char *)arg;
    char server_path[100];
    char client_output[1024], server_output[1024];
    FILE *client_file, *server_file;

    strcpy(server_path, temp_judul);
    strcat(server_path, "/output.txt");
    client_file = fopen(client_path, "r");
    server_file = fopen(server_path, "r");

    memset(client_output, 0, sizeof(client_output));
    while(!feof(client_file)){
        fscanf(client_file, "%s", client_output);
        fscanf(server_file, "%s", server_output);

        if (strcmp(client_output, server_output) != 0){
            ac = 0;
            break;
        }
    }
}

int main(){
    int server_fd, new_socket, valread;
    int client_socket[3], max_clients = 3, activity, i, sd, max_sd;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};
    fd_set readfds;

    for (i = 0; i<max_clients; i++)
        client_socket[i] = 0;

    char username[100][1024];
    char judul[100][1024];
    int problem = 0;


    // login
    int login_successful = 0;
    int user = 0;
    char login_username[100];

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0){
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))){
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 2) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    int index_username = 0;
    int index_judul = 0;

    
    while(1){
        printf("user = %d\n", user);
        FD_ZERO(&readfds);
        FD_SET(server_fd, &readfds);
        max_sd = server_fd;

        for ( i = 0 ; i < max_clients ; i++){
            sd = client_socket[i];
            if(sd > 0)
                FD_SET( sd , &readfds);
            if(sd > max_sd)
                max_sd = sd;
        }

        if (FD_ISSET(server_fd, &readfds) && user < 1){
            if ((new_socket = accept(server_fd, (struct sockaddr *)&address,(socklen_t *)&addrlen)) < 0){
                 perror ("accept");
                 exit(EXIT_FAILURE);
            }

            printf("New connection, socket fd is %d, ip is: %s, port: %d\n", new_socket, inet_ntoa(address.sin_addr), ntohs(address.sin_port));
            user++;
            insertIntoQueue(new_socket);

            for (i = 0; i< max_clients; i++){
                if (client_socket[i] == 0){
                    client_socket[i] = new_socket;
                    break;
                }
            }
        }

        FILE *database = fopen("problem.tsv", "a+");
        FILE *writeFile = fopen("users.txt", "a");
        valread = read(new_socket, buffer, 1024);


        if (strcmp(buffer, "register") == 0){

            // Username
            username:
                memset(buffer, 0, 1024);
                valread = read(new_socket, buffer, 1024);
                char *username_input = buffer;

                int unique = 1;
                for (int i = 0; i<index_username; i++){
                    if (strcmp(username[i], username_input) == 0){
                        // printf("Compare Username = %s\t%s\n", username[i], username_input);
                        unique = 0;
                        char *msg = "Username sudah ada";
                        send(new_socket, msg, strlen(msg), 0);
                        goto username;
                    }
                }

                if (unique){
                    strcpy(username[index_username], username_input);
                    // printf("Username Added = %s\t%s\n", username[index_username], username_input);
                    index_username++;
                    char *msg = "Username Unique";
                    send(new_socket, msg, strlen(msg), 0);
                }

                printf("%s:", buffer);
                fprintf(writeFile, "%s:", username_input);

            //Password
            memset(buffer, 0, 1024);
            valread = read(new_socket, buffer, 1024);
            char *password_input = buffer;
            printf("%s\n", buffer);
            fprintf(writeFile, "%s\n", password_input);
            
        }

        else if ((strcmp(buffer, "login") == 0) && (user <= 1) && (count <= 1)){
            FILE *readFile;
            login_username:
                readFile = fopen("users.txt", "r");
                memset(buffer, 0, 1024);
                valread = read(new_socket, buffer, 1024);

                int exist = 0;
                char data[1024];
                char *check;
                while(!feof(readFile)){
                    fscanf(readFile, "%s", data);
                    check = strtok(data, ":");
                    if (strcmp(buffer, check) == 0){
                        exist = 1;
                        strcpy(login_username, check);
                        check = strtok(NULL, ":");
                        printf("Password = %s\n", check);
                        break;
                    }
                }
                if (exist == 0){
                    char *msg = "Username tidak ada";
                    send(new_socket, msg, strlen(msg), 0);
                    fclose(readFile);
                    goto login_username;
                }else{
                    char *msg = "Username ada";
                    send(new_socket, msg, strlen(msg), 0);
                }
                fclose(readFile);


            login_password:
                memset(buffer, 0, 1024);
                valread = read(new_socket, buffer, 1024);
                if (strcmp(buffer, check) != 0){
                    char *msg = "Password Salah";
                    send(new_socket, msg, strlen(msg), 0);
                    goto login_password;
                }else{
                    login_successful = 1;
                    char *msg = "Login Berhasil\n";
                    send(new_socket, msg, strlen(msg), 0);
                }
        }

        else if ((strcmp(buffer, "add") == 0) && (user <= 1)){
            if (login_successful == 0){
                char *msg = "Mohon Login Terlebih Dahulu";
                send(new_socket, msg, strlen(msg), 0);
            }
            else{
                char *msg = "Add Section";
                send(new_socket, msg, strlen(msg), 0);

                pthread_t tid[4];

                add_judul:
                    memset(buffer, 0, 1024);
                    valread = read(new_socket, buffer, 1024);
                    printf("Judul Problem: %s\t", buffer);
                    char *foldername_input = buffer;
                    int unique = 1;
                    for (int i = 0; i<index_judul; i++){
                        if (strcmp(username[i], foldername_input) == 0){
                            unique = 0;
                            char *msg = "Judul Problem sudah ada";
                            send(new_socket, msg, strlen(msg), 0);
                            goto add_judul;
                        }
                    }
                    if (unique){
                        strcpy(temp_judul, buffer);
                        strcpy(judul[index_judul], foldername_input);
                        index_judul++;

                        char *msg = "Judul Problem Unique";
                        send(new_socket, msg, strlen(msg), 0);

                        pthread_create(&tid[0], NULL, &make_directory, buffer);
                        pthread_join(tid[0], NULL);
                    }
                

                memset(buffer, 0, 1024);
                valread = read(new_socket, buffer, 1024);
                printf("Filepath description.txt: %s\n", buffer);
                pthread_create(&tid[1], NULL, &add_description_file, buffer);
                pthread_join(tid[1], NULL);

                memset(buffer, 0, 1024);
                valread = read(new_socket, buffer, 1024);
                printf("Filepath input.txt: %s\n", buffer);
                pthread_create(&tid[2], NULL, &add_input_file, buffer);
                pthread_join(tid[2], NULL);

                memset(buffer, 0, 1024);
                valread = read(new_socket, buffer, 1024);
                printf("Filepath output.txt: %s\n", buffer);
                pthread_create(&tid[3], NULL, &add_output_file, buffer);
                pthread_join(tid[3], NULL);

                fprintf(database, "%s \t %s\n", temp_judul, login_username);
            }
        }

        else if ((strcmp(buffer, "see") == 0) && (user <= 1)){
            if (login_successful == 0){
                char *msg = "Mohon Login Terlebih Dahulu";
                send(new_socket, msg, strlen(msg), 0);
            }
            else{
                memset(buffer, 0, 1024);
                char response[1024];
                char output[1024];
                memset(response, 0, sizeof(response));
                memset(output, 0, sizeof(output));

                int i = 1;
                while(fgets(response, sizeof(response), database) != NULL){
                    strcat(output, response);
                }
                memset(buffer, 0, 1024);
                send(new_socket, output, strlen(output), 0);
            }
        }

        else if ((strcmp(buffer, "download") == 0) && (user <= 1)){
            if (login_successful == 0){
                char *msg = "Mohon Login Terlebih Dahulu";
                send(new_socket, msg, strlen(msg), 0);
            }
            else{
                char *msg = "Download Section";
                send(new_socket, msg, strlen(msg), 0);
                pthread_t tid;
                
                memset(buffer, 0, 1024);
                valread = read(new_socket, buffer, 1024);
                pthread_create(&tid, NULL, &download_file, buffer);
                pthread_join(tid, NULL);
            }
        }

        else if ((strcmp(buffer, "submit") == 0) && (user <= 1)){
            if (login_successful == 0){
                char *msg = "Mohon Login Terlebih Dahulu";
                send(new_socket, msg, strlen(msg), 0);
            }
            else{
                char *msg = "Submit Section";
                send(new_socket, msg, strlen(msg), 0);
                
                char judul_input[100], output_input[100], client_path[100];
                memset(buffer, 0, 1024);
                valread = read(new_socket, buffer, 1024);
                strcpy(judul_input, buffer);
                printf("Judul Problem = %s\n", judul_input);

                memset(buffer, 0, 1024);
                valread = read(new_socket, buffer, 1024);
                strcpy(output_input, buffer);
                printf("Output File = %s\n", output_input);


                pthread_t tid;
                strcpy(client_path, "../Client/");
                strcat(client_path, output_input);
                strcpy(temp_judul, judul_input);
                pthread_create(&tid, NULL, &check_submit, client_path);
                pthread_join(tid, NULL);

                if (ac == 1){
                    memset(buffer, 0, 1024);
                    send(new_socket, "AC", strlen("AC"), 0);
                }else{
                    memset(buffer, 0, 1024);
                    send(new_socket, "WA", strlen("WA"), 0);
                    ac = 1;
                }
            }
        }
        
        else if (strcmp(buffer, "exit") == 0){
            removeFromQueue();
            printf("Host disconnected, ip %s, port %d \n", inet_ntoa(address.sin_addr), ntohs(address.sin_port));
            close(sd);
            client_socket[i] = 0;
            user--;
        }
        
        else{
            char *msg = "Bad Request";
            send(new_socket, msg, strlen(msg), 0);
        }

        memset(buffer, 0, 1024);

        fclose(writeFile);
        fclose(database);

        
    }

}
