# soal-shift-sisop-modul-3-ITA12-2022
## Anggota Kelompok
1. Muhammad Jovan Adiwijaya Yanuarsyah (5027201025)
2. Made Gede Krisna Wangsa (5027201047)
3. Nida'ul Faizah (5027201064)

## Description
Laporan ini dibuat dengan tujuan untuk menjelaskan pengerjaan serta permasalahan yang kami alami dalam pengerjaan soal shift sistem operasi modul 3 tahun 2022.

## Soal Shift 1 (revisi)
Novak adalah seorang mahasiswa biasa yang terlalu sering berselancar di internet. Pada suatu saat, Ia menemukan sebuah informasi bahwa ada suatu situs yang tidak memiliki pengguna. Ia mendownload berbagai informasi yang ada dari situs tersebut dan menemukan sebuah file dengan tulisan yang tidak jelas. Setelah diperhatikan lagi, kode tersebut berformat base64. Ia lalu meminta kepada anda untuk membuat program untuk memecahkan kode-kode di dalam file yang Ia simpan di drive dengan cara decoding dengan base 64. Agar lebih cepat, Ia sarankan untuk menggunakan thread.
## A) (revisi)
Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan nama quote untuk file zip quote.zip dan music untuk file zip music.zip. Unzip ini dilakukan dengan bersamaan menggunakan thread.

Soal tersebut meminta kita untuk melakukan download .zip yang berapa pada link yang disediakan. Dengan menggunakan `exec` serta argumen `/bin/wget` , kita dapat melakukan download sebuah file dari alamat website yang diberikan. Kita juga perlu membuat folder `music`dan folder `quote` untuk menampung hasil dari zip tersebut.
```c
void zip_download(){
    pid_t child_id;
    int status;

    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if (child_id == 0) {
        // this is child
        char *argv[] = {"wget", "-q", "https://drive.google.com/u/0/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download", "-O", "quote.zip", NULL};
        execv("/bin/wget", argv);
        exit(0);
    } else {
        // this is parent
        while ((wait(&status)) > 0);

        char *argv[] = {"wget", "-q", "https://drive.google.com/u/0/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download", "-O", "music.zip", NULL};
        execv("/bin/wget", argv);
        exit(0);
    }
}

void unzip_file(char* dir, char* pass){
    pid_t child_id;
    int status;

    char zip_file[100];
    sprintf(zip_file, "%s.zip", dir);

    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if(child_id == 0){
	if(pass == NULL){
            char* argv[] = {"unzip", "-q","-n", zip_file, "-d", dir, NULL};
            execv("/bin/unzip",argv);
            exit(0);
        }
	
	else {
            char* argv[] = {"unzip", "-P", pass, zip_file, NULL};
            execv("/bin/unzip",argv);
            exit(0);
	}
    }
    else while ((wait(&status)) > 0);
}

void make_dir(char* dir){
    pid_t child_id;
    int status;

    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if(child_id == 0){
        char *argv[] = {"mkdir", "-p", dir, NULL};
        execv("/bin/mkdir", argv);
        exit(0);
    }
    else while ((wait(&status)) > 0);  
}

```
## B) (revisi)
Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu file .txt yang baru untuk masing-masing folder (Hasilnya nanti ada dua file .txt) pada saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan music.txt. Masing-masing kalimat dipisahkan dengan newline/enter.
Pertama kita perlu decode file berisi base 64 dengan fungsi-fungsi dibawah ini:
```c
const char b64chars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

int b64invs[] = { 62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58,
	59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5,
	6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
	21, 22, 23, 24, 25, -1, -1, -1, -1, -1, -1, 26, 27, 28,
	29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42,
	43, 44, 45, 46, 47, 48, 49, 50, 51 };

size_t b64_decoded_size(const char *in)
{
	size_t len;
	size_t ret;
	size_t i;

	if (in == NULL)
		return 0;

	len = strlen(in);
	ret = len / 4 * 3;

	for (i=len; i-->0; ) {
		if (in[i] == '=') {
			ret--;
		} else {
			break;
		}
	}

	return ret;
}

void b64_generate_decode_table()
{
	int    inv[80];
	size_t i;

	memset(inv, -1, sizeof(inv));
	for (i=0; i<sizeof(b64chars)-1; i++) {
		inv[b64chars[i]-43] = i;
	}
}

int b64_isvalidchar(char c)
{
	if (c >= '0' && c <= '9')
		return 1;
	if (c >= 'A' && c <= 'Z')
		return 1;
	if (c >= 'a' && c <= 'z')
		return 1;
	if (c == '+' || c == '/' || c == '=')
		return 1;
	return 0;
}

int b64_decode(const char *in, unsigned char *out, size_t outlen)
{
	size_t len;
	size_t i;
	size_t j;
	int    v;

	if (in == NULL || out == NULL)
		return 0;

	len = strlen(in);
	if (outlen < b64_decoded_size(in) || len % 4 != 0)
		return 0;

	for (i=0; i<len; i++) {
		if (!b64_isvalidchar(in[i])) {
			return 0;
		}
	}

	for (i=0, j=0; i<len; i+=4, j+=3) {
		v = b64invs[in[i]-43];
		v = (v << 6) | b64invs[in[i+1]-43];
		v = in[i+2]=='=' ? v << 6 : (v << 6) | b64invs[in[i+2]-43];
		v = in[i+3]=='=' ? v << 6 : (v << 6) | b64invs[in[i+3]-43];

		out[j] = (v >> 16) & 0xFF;
		if (in[i+2] != '=')
			out[j+1] = (v >> 8) & 0xFF;
		if (in[i+3] != '=')
			out[j+2] = v & 0xFF;
	}

	return 1;
}
```
setelah itu, dapat kita pindahkan hasilnya ke dalam dua text file untuk tiap folder `music` dan `quote`

```c
void *decode(void *file){
    char *filename, buffer[1024], *decoded, loc[20];
    filename = (char *) file;
    size_t len;
    FILE *fptr;
    fptr = fopen(filename, "r");
    fscanf(fptr, "%s", buffer);
    fclose(fptr);

    len = b64_decoded_size(buffer) + 1;
    decoded = (char*) malloc(len*sizeof(char));

    if(b64_decode(buffer, (unsigned char *) decoded, len)){
        decoded[len] = '\0';
        if(filename[0] == 'm') strcpy(loc, "music.txt");
        else if(filename[0] == 'q') strcpy(loc, "quote.txt");
        fptr = fopen(loc, "a");
        fprintf(fptr, "%s\n", decoded);
        fclose(fptr);
    }
}
```
## C) (revisi)
Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil.
Kita dapat melakukan pemindahan file dengan membuka file yang akan dipindah dengan permission `r` serta membuka directory yang akan dituju dengan permission `w`. Setelah itu kita akan menggunakan loop `while` untuk mengambil semua isi yang hendak dipindah hingga terjadi EOF (End Of File).
```c
void move_file(char* dest, char* src){
    FILE *fptr1, *fptr2;
    char temp;

    fptr1 = fopen(src, "r");
    fptr2 = fopen(dest, "w");

    while(fscanf(fptr1, "%c", &temp) != EOF) fprintf(fptr2, "%c", temp);

    fclose(fptr1);
    fclose(fptr2);
}

move_file("hasil/music.txt", "music.txt");
move_file("hasil/quote.txt", "quote.txt");

```

## D) (revisi)
Folder `hasil` zip menjadi file `hasil.zip` dengan password 'mihinomenest[Nama user]'. (contoh password : mihinomenestnovak)
```c
void zip_file(char *dir){
    pid_t child_id;
    int status;

    char zip_file[100];
    sprintf(zip_file, "%s.zip", dir);

    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if(child_id == 0){
        char* argv[] = {"zip", "-P","mihinomenestlax", "-r", zip_file, dir, NULL};
        execv("/bin/zip",argv);
        exit(0);
    }
    else while ((wait(&status)) > 0);
}

zip_file("hasil");
```
## E) (revisi)
Karena ada yang kurang, kalian diminta untuk unzip file hasil.zip dan buat file no.txt dengan tulisan 'No' pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt menjadi hasil.zip, dengan password yang sama seperti sebelumnya.
```c
void unzip_file(char* dir, char* pass){
    pid_t child_id;
    int status;

    char zip_file[100];
    sprintf(zip_file, "%s.zip", dir);

    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if(child_id == 0){
	if(pass == NULL){
            char* argv[] = {"unzip", "-q","-n", zip_file, "-d", dir, NULL};
            execv("/bin/unzip",argv);
            exit(0);
        }
	
	else {
            char* argv[] = {"unzip", "-P", pass, zip_file, NULL};
            execv("/bin/unzip",argv);
            exit(0);
	}
    }
    else while ((wait(&status)) > 0);
}

void zip_file(char *dir){
    pid_t child_id;
    int status;

    char zip_file[100];
    sprintf(zip_file, "%s.zip", dir);

    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if(child_id == 0){
        char* argv[] = {"zip", "-P","mihinomenestdaniel", "-r", zip_file, dir, NULL};
        execv("/bin/zip",argv);
        exit(0);
    }
    else while ((wait(&status)) > 0);
}

unzip_file("hasil.zip", "mihinomenestdaniel");
create_txt("hasil/no.txt");
zip_file("hasil");
```

## Soal Shift 2
Bluemary adalah seorang Top Global 1 di salah satu platform online judge. Suatu hari Ia ingin membuat online judge nya sendiri, namun dikarenakan Ia sibuk untuk mempertahankan top global nya, maka Ia meminta kamu untuk membantunya dalam membuat online judge sederhana. Online judge sederhana akan dibuat dengan sistem client-server dengan beberapa kriteria sebagai berikut:

## A) (revisi)
Pada saat client terhubung ke server, terdapat dua pilihan pertama yaitu register dan login. Jika memilih register, client akan diminta input id dan passwordnya untuk dikirimkan ke server. Data input akan disimpan ke file users.txt dengan format username:password. Jika client memilih login, server juga akan meminta client untuk input id dan passwordnya lalu server akan mencari data di users.txt yang sesuai dengan input client. Jika data yang sesuai ditemukan, maka client dapat login dan dapat menggunakan command-command yang ada pada sistem. Jika tidak maka server akan menolak login client. Username dan password memiliki kriteria sebagai berikut:
- Username unique (tidak boleh ada user yang memiliki username yang sama)
- Password minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil

Server akan menerima kiriman dari client saat pertama kali dijalankan. Apabila client mengirimkan `register` maka masuk ke dalam proses register di server, begitupun dengan `login`.
**server**

```c
        if (strcmp(buffer, "register") == 0){

            // Username
            username:
                memset(buffer, 0, 1024);
                valread = read(new_socket, buffer, 1024);
                char *username_input = buffer;

                int unique = 1;
                for (int i = 0; i<index_username; i++){
                    if (strcmp(username[i], username_input) == 0){
                        // printf("Compare Username = %s\t%s\n", username[i], username_input);
                        unique = 0;
                        char *msg = "Username sudah ada";
                        send(new_socket, msg, strlen(msg), 0);
                        goto username;
                    }
                }

                if (unique){
                    strcpy(username[index_username], username_input);
                    // printf("Username Added = %s\t%s\n", username[index_username], username_input);
                    index_username++;
                    char *msg = "Username Unique";
                    send(new_socket, msg, strlen(msg), 0);
                }

                printf("%s:", buffer);
                fprintf(writeFile, "%s:", username_input);

            //Password
            memset(buffer, 0, 1024);
            valread = read(new_socket, buffer, 1024);
            char *password_input = buffer;
            printf("%s\n", buffer);
            fprintf(writeFile, "%s\n", password_input);
            
        }

        else if ((strcmp(buffer, "login") == 0) && (user == 0)){
            FILE *readFile;
            login_username:
                readFile = fopen("users.txt", "r");
                memset(buffer, 0, 1024);
                valread = read(new_socket, buffer, 1024);

                int exist = 0;
                char data[1024];
                char *check;
                while(!feof(readFile)){
                    fscanf(readFile, "%s", data);
                    check = strtok(data, ":");
                    if (strcmp(buffer, check) == 0){
                        exist = 1;
                        strcpy(login_username, check);
                        check = strtok(NULL, ":");
                        printf("Password = %s\n", check);
                        break;
                    }
                }
                if (exist == 0){
                    char *msg = "Username tidak ada";
                    send(new_socket, msg, strlen(msg), 0);
                    fclose(readFile);
                    goto login_username;
                }else{
                    char *msg = "Username ada";
                    send(new_socket, msg, strlen(msg), 0);
                }
                fclose(readFile);


            login_password:
                memset(buffer, 0, 1024);
                valread = read(new_socket, buffer, 1024);
                if (strcmp(buffer, check) != 0){
                    char *msg = "Password Salah";
                    send(new_socket, msg, strlen(msg), 0);
                    goto login_password;
                }else{
                    login_successful = 1;
                    char *msg = "Login Berhasil\n";
                    send(new_socket, msg, strlen(msg), 0);
                    user++;
                }
        }

```
- Setiap register dilakukan pengecekan username. Apabila username dari client sudah ada maka tidak bisa dilanjutkan/
- Apabila username dari client belum ada maka register dapat dilanjutkan
- Saat login, username dari client harus username yang tersedia di users.txt dan saat memasukan password di cek kesesuaian dengan yang ada di file users.txt

**client**
```c
        if (strcmp(input, "register") <= 0){

            username:
                memset(buffer, 0, 1024);
                printf("Username: ");
                scanf("%s", input);
                send(sock, input, strlen(input), 0);

                valread = read(sock, buffer, 1024);
                if (strcmp(buffer, "Username sudah ada") == 0){
                    printf("%s\n", buffer);
                    goto username;
                }else
                    printf("%s\n", buffer);

            int uppercase, lowercase, number;
            password:
                memset(buffer, 0, 1024);
                uppercase = 0;
                lowercase = 0;
                number = 0;
                printf("Password: ");
                scanf("%s", input);
                if (strlen(input) < 6){
                    printf("\nPassword minimal terdiri dari 6 karakter\n\n");
                    goto password;
                }
                for (int i = 0; i<strlen(input); i++){
                    // printf("%c ", input[i]);
                    if (input[i]>=48 && input[i]<=57)
                        number = 1;
                    if (input[i]>=65 && input[i]<=90)
                        uppercase = 1;
                    if (input[i]>=97 && input[i]<=122)
                        lowercase = 1;
                    // printf("number = %d, uppercase = %d, lowercase = %d\n", number, uppercase, lowercase);
                }
                if (number == 0 || uppercase == 0 || lowercase == 0){
                    printf("\nPassword minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil\n\n");
                    goto password;
                }
                send(sock, input, strlen(input), 0);
                printf("Akun terdaftar\n");
        }

        else if (strcmp(input, "login") <= 0){
            
            login_username:
                memset(buffer, 0, 1024);
                printf("Username: ");
                scanf("%s", input);
                send(sock, input, strlen(input), 0);

                valread = read(sock, buffer, 1024);
                if (strcmp(buffer, "Username tidak ada") == 0){
                    printf("%s\n", buffer);
                    goto login_username;
                }

            login_password:
                memset(buffer, 0, 1024);
                printf("Password: ");
                scanf("%s", input);
                send(sock, input, strlen(input), 0);
                valread = read(sock, buffer, 1024);
                if (strcmp(buffer, "Password Salah") == 0){
                    printf("%s\n", buffer);
                    goto login_password;
                }else
                    printf("%s\n", buffer);
        }

```
- Pengecekan syarat password dilakukan di sisi client karena tidak memerlukan data yang ada di server.

## B) dan C) (revisi)
b)	Sistem memiliki sebuah database pada server untuk menampung problem atau soal-soal yang ada pada online judge. Database ini bernama problems.tsv yang terdiri dari judul problem dan author problem (berupa username dari author), yang dipisah dengan \t. File otomatis dibuat saat server dijalankan.
c)  c.	Client yang telah login, dapat memasukkan command yaitu ‘add’ yang berfungsi untuk menambahkan problem/soal baru pada sistem. Saat client menginputkan command tersebut, server akan meminta beberapa input yaitu:
- Judul problem (unique, tidak boleh ada yang sama dengan problem lain)
- Path file description.txt pada client (file ini berisi deskripsi atau penjelasan problem)
- Path file input.txt pada client (file ini berguna sebagai input testcase untuk menyelesaikan problem)
- Path file output.txt pada client (file ini berguna untuk melakukan pengecekan pada submission client terhadap problem)

Fitur `add` akan bisa dilakukan ketika user sudah login. Hal ini ditandai dengan variabel `login_successful` dan `user`.
**server**
```c
        else if ((strcmp(buffer, "add") <= 0) && (user <= 1)){
            if (login_successful == 0){
                char *msg = "Mohon Login Terlebih Dahulu";
                send(new_socket, msg, strlen(msg), 0);
            }
            else{
                char *msg = "Add Section";
                send(new_socket, msg, strlen(msg), 0);

                pthread_t tid[4];

                add_judul:
                    memset(buffer, 0, 1024);
                    valread = read(new_socket, buffer, 1024);
                    printf("Judul Problem: %s\t", buffer);
                    char *foldername_input = buffer;
                    int unique = 1;
                    for (int i = 0; i<index_judul; i++){
                        if (strcmp(username[i], foldername_input) == 0){
                            unique = 0;
                            char *msg = "Judul Problem sudah ada";
                            send(new_socket, msg, strlen(msg), 0);
                            goto add_judul;
                        }
                    }
                    if (unique){
                        strcpy(temp_judul, buffer);
                        strcpy(judul[index_judul], foldername_input);
                        index_judul++;

                        char *msg = "Judul Problem Unique";
                        send(new_socket, msg, strlen(msg), 0);

                        pthread_create(&tid[0], NULL, &make_directory, buffer);
                        pthread_join(tid[0], NULL);
                    }
                

                memset(buffer, 0, 1024);
                valread = read(new_socket, buffer, 1024);
                printf("Filepath description.txt: %s\n", buffer);
                pthread_create(&tid[1], NULL, &add_description_file, buffer);
                pthread_join(tid[1], NULL);

                memset(buffer, 0, 1024);
                valread = read(new_socket, buffer, 1024);
                printf("Filepath input.txt: %s\n", buffer);
                pthread_create(&tid[2], NULL, &add_input_file, buffer);
                pthread_join(tid[2], NULL);

                memset(buffer, 0, 1024);
                valread = read(new_socket, buffer, 1024);
                printf("Filepath output.txt: %s\n", buffer);
                pthread_create(&tid[3], NULL, &add_output_file, buffer);
                pthread_join(tid[3], NULL);

                fprintf(database, "%s \t %s\n", temp_judul, login_username);
            }
        }

```
**client**
```c
        else if (strcmp(input, "add") == 0){
            memset(buffer, 0, 1024);
            valread = read(sock, buffer, 1024);

            if (strcmp(buffer, "Mohon Login Terlebih Dahulu") == 0){
                printf("%s\n", buffer);
            }else{
                add_judul:
                    memset(buffer, 0, 1024);
                    scanf("%s", input);
                    send(sock, input, strlen(input), 0);
                    valread = read(sock, buffer, 1024);
                    if (strcmp(buffer, "Judul Problem sudah ada") == 0){
                        printf("%s\n", buffer);
                        goto add_judul;
                    }else{
                        printf("%s\n", buffer);
                    }
                
                add_filepath_description:
                    memset(buffer, 0, 1024);
                    scanf("%s", input);
                    send(sock, input, strlen(input), 0);

                add_filepath_input:
                    memset(buffer, 0, 1024);
                    scanf("%s", input);
                    send(sock, input, strlen(input), 0);

                add_filepath_output:
                    memset(buffer, 0, 1024);
                    scanf("%s", input);
                    send(sock, input, strlen(input), 0);
    
            }
        }

```
- Judul problem harus dicek terlebih dahulu karena harus unique
- Input nama file harus beserta dengan extensinya
- Diakhir tambahkan judul dan username di database problem.tsv

## D) (revisi)
Client yang telah login, dapat memasukkan command ‘see’ yang berguna untuk menampilkan seluruh judul problem yang ada beserta authornya(author merupakan username client yang menambahkan problem tersebut). 
Penyelesaian dilakukan dengan membaca file problems.tsv dan diatur format outputnya di client
**server**
```c
        else if ((strcmp(buffer, "see") == 0) && (user <= 1)){
            if (login_successful == 0){
                char *msg = "Mohon Login Terlebih Dahulu";
                send(new_socket, msg, strlen(msg), 0);
            }
            else{
                memset(buffer, 0, 1024);
                char response[1024];
                char output[1024];
                memset(response, 0, sizeof(response));
                memset(output, 0, sizeof(output));

                int i = 1;
                while(fgets(response, sizeof(response), database) != NULL){
                    strcat(output, response);
                }
                memset(buffer, 0, 1024);
                send(new_socket, output, strlen(output), 0);
            }
        }


```
**client**
```c
        else if (strcmp(input, "see") == 0){
            if (strcmp(buffer, "Mohon Login Terlebih Dahulu") == 0){
                printf("%s\n", buffer);
            }else{
                memset(buffer, 0, 1024);
                valread = read(sock, buffer, 1024);
                char output[256];
                char temp[256];

                memset(output, 0, sizeof(output));
                memset(temp, 0, sizeof(temp));

                strcpy(temp, buffer);

                for (int i = 0; i<strlen(temp); i++){
                    if (temp[i] == ' ') continue;
                    else if (temp[i+1] == ' ' && (temp[i] != ' ' && temp[i] != '\t')){
                        strncat(output, &temp[i], 1);
                        strcat(output, " by ");
                        continue;
                    }
                    strncat(output, &temp[i], 1);
                }
                printf("%s\n", output);
            }
            
        }


```

## E) (revisi)
Client yang telah login, dapat memasukkan command `download <judul-problem>` yang berguna untuk mendownload file description.txt dan input.txt yang berada pada folder pada server dengan nama yang sesuai dengan argumen kedua pada command yaitu <judul-problem>. Kedua file tersebut akan disimpan ke folder dengan nama <judul-problem> di client.

Proses download diawali dengan membaca file yang ada di server lalu di copy ke client. Berikut code dalam implementasi download

**server**
```c
        else if ((strcmp(buffer, "download") == 0) && (user == 0)){
            if (login_successful == 0){
                char *msg = "Mohon Login Terlebih Dahulu";
                send(new_socket, msg, strlen(msg), 0);
            }
            else{
                char *msg = "Download Section";
                send(new_socket, msg, strlen(msg), 0);
                pthread_t tid;
                
                memset(buffer, 0, 1024);
                valread = read(new_socket, buffer, 1024);
                pthread_create(&tid, NULL, &download_file, buffer);
                pthread_join(tid, NULL);
            }
        }

```
**client**
```c
        else if (strcmp(input, "download") == 0){
            memset(buffer, 0, 1024);
            valread = read(sock, buffer, 1024);

            if (strcmp(buffer, "Mohon Login Terlebih Dahulu") == 0){
                printf("%s\n", buffer);
            }else{
                printf("%s\n", buffer);
                memset(buffer, 0, 1024);
                printf("Judul Problem: ");
                scanf("%s", input);
                send(sock, input, strlen(input), 0);
            }
        }

```

## F) (revisi)
Client yang telah login, dapat memasukkan command ‘submit <judul-problem> <path-file-output.txt>’.  Command ini berguna untuk melakukan submit jawaban dari client terhadap problem tertentu. Algoritma yang dijalankan adalah client akan mengirimkan file output.txt nya melalui argumen ke 3 pada command, lalu server akan menerima dan membandingkan isi file output.txt yang telah dikirimkan oleh client dan output.txt yang ada pada folder dengan nama yang sesuai dengan argumen ke 2 pada command. Jika file yang dibandingkan sama, maka server akan mengirimkan pesan “AC” dan jika tidak maka server akan mengeluarkan pesan “WA”.

Penyelesaian dilakukan dengan read kedua file dan membandingkan tiap baris

**server**
```c
        else if ((strcmp(buffer, "submit") == 0) && (user <= 1)){
            if (login_successful == 0){
                char *msg = "Mohon Login Terlebih Dahulu";
                send(new_socket, msg, strlen(msg), 0);
            }
            else{
                char *msg = "Submit Section";
                send(new_socket, msg, strlen(msg), 0);
                
                char judul_input[100], output_input[100], client_path[100];
                memset(buffer, 0, 1024);
                valread = read(new_socket, buffer, 1024);
                strcpy(judul_input, buffer);
                printf("Judul Problem = %s\n", judul_input);

                memset(buffer, 0, 1024);
                valread = read(new_socket, buffer, 1024);
                strcpy(output_input, buffer);
                printf("Output File = %s\n", output_input);


                pthread_t tid;
                strcpy(client_path, "../Client/");
                strcat(client_path, output_input);
                strcpy(temp_judul, judul_input);
                pthread_create(&tid, NULL, &check_submit, client_path);
                pthread_join(tid, NULL);

                if (ac == 1){
                    memset(buffer, 0, 1024);
                    send(new_socket, "AC", strlen("AC"), 0);
                }else{
                    memset(buffer, 0, 1024);
                    send(new_socket, "WA", strlen("WA"), 0);
                    ac = 1;
                }
            }
        }

```

**client**
```c
        else if (strcmp(input, "submit") == 0){
            memset(buffer, 0, 1024);
            valread = read(sock, buffer, 1024);
            if (strcmp(buffer, "Mohon Login Terlebih Dahulu") == 0){
                printf("%s\n", buffer);
            }else{
                printf("Judul Problem: ");
                scanf("%s", input);
                send(sock, input, strlen(input), 0);

                printf("Output File: ");
                scanf("%s", input);
                send(sock, input, strlen(input), 0);

                memset(buffer, 0, 1024);
                valread = read(sock, buffer, 1024);
                printf("%s\n\n", buffer);
                
            }
        }


```

## G)
Server dapat menangani multiple-connection. Dimana jika terdapat 2 atau lebih client yang terhubung ke server, maka harus menunggu sampai client pertama keluar untuk bisa melakukan login dan mengakses aplikasinya.

Penyelesaian dilakukan dengan queue dan mengatur agar client lain tidak bisa mengakses socket server (login) apabila client pertama belum keluar dari server (log out)

**server**
```c
    int queue[3];
    int count = 0;

    void insertIntoQueue(int x){
        if (count == 3){
            fprintf(stderr, "No more space for client\n");
        }
        queue[count] = x;
        count++;
    }

    int removeFromQueue(){
        if (count == 0){
            fprintf(stderr, "No elements to extract from queue\n");
            return -1;
        }
        int res = queue[0];
        int i;
        for (i = 0; i<count-1; i++){
            queue[i] = queue[i+1];
        }
        count--;
        return res;
    }

        ...

        if (FD_ISSET(server_fd, &readfds) && user < 1){
            if ((new_socket = accept(server_fd, (struct sockaddr *)&address,(socklen_t *)&addrlen)) < 0){
                 perror ("accept");
                 exit(EXIT_FAILURE);
            }

            printf("New connection, socket fd is %d, ip is: %s, port: %d\n", new_socket, inet_ntoa(address.sin_addr), ntohs(address.sin_port));
            user++;
            insertIntoQueue(new_socket);

            for (i = 0; i< max_clients; i++){
                if (client_socket[i] == 0){
                    client_socket[i] = new_socket;
                    break;
                }
            }
        }

```


## Soal Shift 3

Nami adalah seorang pengoleksi harta karun handal. Karena Nami memiliki waktu luang, Nami pun mencoba merapikan harta karun yang dimilikinya berdasarkan jenis/tipe/kategori/ekstensi harta karunnya. Setelah harta karunnya berhasil dikategorikan, Nami pun mengirimkan harta karun tersebut ke kampung halamannya.

Pertama-tama yang perlu dilakukan adalah mendapatkan ekstensi file dan mengkategorikan berdasarkan ekstensi secara rekursif, mengiterasi file. Jika file tidak memiliki ekstensi, file akan disimpan di unknown dan file hidden masuk folder hidden menggunakan thread dengan beberapa catatan berikut:
Kategori folder tidak dibuat secara manual, harus melalui program C
Program ini tidak case sensitive. Contoh: JPG dan jpg adalah sama
Jika ekstensi lebih dari satu (contoh “.tar.gz”) maka akan masuk ke folder dengan titik terdepan (contoh “tar.gz”)
Dilarang juga menggunakan fork, exec dan system()

